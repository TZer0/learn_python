import unittest

"""
Create a function called isMoreThanTen that takes one argument. If the argument is greater than 10 (but not equal 
to 10), return True, if not, return False
"""


class Tests(unittest.TestCase):
    def test_5(self):
        self.assertEqual(False, isMoreThanTen(5))

    def test_10(self):
        self.assertEqual(False, isMoreThanTen(10))

    def test_min11(self):
        self.assertEqual(False, isMoreThanTen(-11))

    def test_11(self):
        self.assertEqual(True, isMoreThanTen(11))


if __name__ == '__main__':
    unittest.main()
