import unittest

"""
Below, the function double() takes one argument and returns the double of that argument.

Write a function that's called doublePlusOne that takes an argument, doubles it using the double-function and then 
adds one. 
"""


def double(x):
    return x * 2


class Tests(unittest.TestCase):
    def test_double1(self):
        self.assertEqual(21, doublePlusOne(10))

    def test_double2(self):
        self.assertEqual(11, doublePlusOne(5))


if __name__ == '__main__':
    unittest.main()
