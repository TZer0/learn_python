import unittest

"""
Below, there's a function called "callMe".

Call this function.

For all these tasks, do not change anything after the line that begins with "class Tests"
"""

wasCalled = False


def callMe():
    global wasCalled
    print("You called me!")
    wasCalled = True


"""
Solve the task below this line
"""

"""
But above this line
"""


class Tests(unittest.TestCase):
    def test_func(self):
        self.assertTrue(wasCalled)


if __name__ == '__main__':
    unittest.main()
