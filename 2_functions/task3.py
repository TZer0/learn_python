import unittest

"""
The function below returns the value 5, make it return 10 instead.
"""


def func():
    return 5


class Tests(unittest.TestCase):
    def test_func(self):
        self.assertEqual(10, func())


if __name__ == '__main__':
    unittest.main()
