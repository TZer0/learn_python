import unittest

"""
Define a function called f that takes no arguments and returns the word "yes"
"""


class Tests(unittest.TestCase):
    def test_f(self):
        self.assertEqual("yes", f())


if __name__ == '__main__':
    unittest.main()
