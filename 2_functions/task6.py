import unittest

"""
Define a function called g that takes an argument and returns this parameter + 1
"""


class Tests(unittest.TestCase):
    def test_g6(self):
        self.assertEqual(6, g(5))

    def test_g10(self):
        self.assertEqual(11, g(10))


if __name__ == '__main__':
    unittest.main()
