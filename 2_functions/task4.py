import unittest

"""
Below, there's a function called "addToList" that takes an argument

You need to call it four times, with the following arguments (in this order):
42
"good"
"foo"
"bar"
"""

myList = []


def addToList(v):
    global myList
    myList.append(v)


"""
Solve this task below this line
"""


class Tests(unittest.TestCase):
    def test_add42(self):
        self.assertEqual(42, myList[0])

    def test_addGood(self):
        self.assertEqual("good", myList[1])

    def test_addFoo(self):
        self.assertEqual("foo", myList[2])

    def test_addBar(self):
        self.assertEqual("bar", myList[3])

    def test_len(self):
        self.assertEqual(4, len(myList))


if __name__ == '__main__':
    unittest.main()
