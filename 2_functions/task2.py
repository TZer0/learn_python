import unittest

"""
Below, there is a function called "what" that takes one argument.

Call this function with the number 7 as an argument and store the result as resultOf7
Call this function with the number 10 as an argument and store the result as resultOf10

Keep in mind that the names are case sensitive

For all these tasks, do not change anything after the line that begins with "class Tests"
"""


def what(x):
    return x + (x % 4) * x


class Tests(unittest.TestCase):
    def test_7(self):
        self.assertEqual(resultOf7, 28)

    def test_10(self):
        self.assertEqual(resultOf10, 30)


if __name__ == '__main__':
    unittest.main()
