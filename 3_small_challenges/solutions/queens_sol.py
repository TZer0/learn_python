"""
Spoilers for all the tasks below, don't use.
"""


def queens(n):
    ret = []
    queens_rec([], n, ret)
    return ret


def queens_rec(seq, n, sol):
    for i in range(n):
        if n == len(seq):
            sol.append(seq.copy())
            return
        if i in seq:
            continue

        skip = False
        for j in range(len(seq)):
            diff = (len(seq) - j)
            if seq[j] + diff == i or seq[j] - diff == i:
                skip = True
                break
        if skip:
            continue
        seq.append(i)
        queens_rec(seq, n, sol)
        seq.pop()
