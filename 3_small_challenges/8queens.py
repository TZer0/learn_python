import unittest

"""

Programmatically solve the 8 queens puzzle

https://en.wikipedia.org/wiki/Eight_queens_puzzle

Your function needs to output a list containing multiple lists. The inner-most lists contain the Y-positions (
zero-indexed) of the queen-pieces on the board from left to right (but honestly, it could be right to left and this 
would still yield the same results). 

Find all solutions, including rotated versions.

"""


def queens():
    return []


"""
Solve above this line
"""

from .solutions import queens_sol


class Tests(unittest.TestCase):
    solutions = queens()
    actual_solutions = queens_sol.queens(8)

    def test_num_solutions(self):
        self.assertEqual(92, len(self.solutions))

    def test_correct_solutions(self):
        for aSol in self.actual_solutions:
            self.assertTrue(aSol in self.solutions, "Missing: " + str(aSol))


if __name__ == '__main__':
    unittest.main()
