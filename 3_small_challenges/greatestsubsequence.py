"""
Modify the function below to return the value of the subsequence with the highest value in the list that is passed to it.

Expected results:
    greatestSubSequence([30, 40, -100, 10, 10, 10]) => 70 (30 + 40)
    greatestSubSequence([-10, 10, 10, -5, 10, -50, 20]) => 25 (10 + 10 + -5 + 10)
    greatestSubSequence([1, 2, 3]) => 6
    greatestSubSequence([-8, -6, -100]) => -6
    greatestSubSequence([-1]) => -1

For the already more knowledgeable: this can be solved with a single-for-loop (with some ifs obviously) or in O(N)
complexity.

"""
import unittest


def greatestSubSequence(numbers):
    return 0


class Tests(unittest.TestCase):
    def test_case_regular(self):
        self.assertEqual(70, greatestSubSequence([30, 40, -100, 10, 10, 10]))

    def test_case_complicated(self):
        self.assertEqual(25, greatestSubSequence([-10, 10, 10, -5, 10, -50, 20]))

    def test_case_all_positive(self):
        self.assertEqual(6, greatestSubSequence([1, 2, 3]))

    def test_case_all_negative(self):
        self.assertEqual(-6, greatestSubSequence([-8, -6, -100]))

    def test_case_one_numbers(self):
        self.assertEqual(-1, greatestSubSequence([-1]))


if __name__ == '__main__':
    unittest.main()
