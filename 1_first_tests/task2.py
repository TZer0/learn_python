import unittest

"""

Task 2:
* Calculate the sum of all numbers between 1 and 15 using a while loop and store the result as sumOfNumbers
* Below, there is a function called double. It returns just x, but it should instead return two times x. Fix it.

Verify that you've done this correctly by running the following from the command line:
python task2.py

Description of needed functionality

While-loops

This is a while-loop that will print the numbers from 0 to 10

i = 0
while i < 10:
    print(i)
    i += 1

The way it works is that the body of the loop (the print and the += statement) are executed as long as i is less than 10.

Functions

This is a function and a function call.

def myFunction(x):
    return x + 3

y = 10
print(myFunction(y))

myFunction is the name of the function. x is a parameter to the functionn, so when it is called using myFunction(y), 
the value of y (10) is inserted as x inside the function. return passes a value back to where the function was called 
and exits the function. So in this case, it will return 13 (10 + 3) to the print-function. 

"""

"""
Solve the task below this line
"""


def double(x):
    return x


"""
But above this line
"""


class TaskTests(unittest.TestCase):

    def test_isSumOfNumbersSet(self):
        self.assertEqual(120, sumOfNumbers)

    def test_double_four(self):
        self.assertEqual(8, double(4))

    def test_double_test(self):
        self.assertEqual("testtest", double("test"))


if __name__ == '__main__':
    unittest.main()
