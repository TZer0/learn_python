import unittest

"""

Task 4: lists!

* Create a list called myList that contains three elements: "zebra", 42, and "!!!"
* Create a function called headTail that takes a list and returns the first and the last element in a list.

headTail([1,2,3,4,5]) should return [1, 5]
headTail(["bear", "zebra", "seagul"]) should return ["bear", "seagul"]

Example, this is a list that contains 4 different numbers and the word "bear":
aList = [3, 5, 7, 9, "bear"]
"""

"""
Solve the task below this line
"""

"""
But above this line
"""


class TaskTests(unittest.TestCase):
    def test_myList_zebra(self):
        self.assertEqual(myList[0], "zebra")

    def test_myList_42(self):
        self.assertEqual(myList[1], 42)

    def test_myList_42(self):
        self.assertEqual(myList[2], "!!!")

    def test_myList_len3(self):
        self.assertEqual(len(myList), 3)

    def test_headTail1(self):
        self.assertEqual(headTail([1, 2, 3, 4, 5]), [1, 5])

    def test_headTail2(self):
        self.assertEqual(headTail([1]), [1, 1])

    def test_headTail3(self):
        self.assertEqual(headTail(["bear", "zebra"]), ["bear", "zebra"])


if __name__ == '__main__':
    unittest.main()
