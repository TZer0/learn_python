import unittest

"""

Task 5: lists extended

* Create a function called matchIt that takes two arguments.
The first argument will be a list, the second argument will be a value.

The function should return the number of occurences of the value in the list.

Examples:
matchIt([1, 2, 3, 4, 1, 1], 1) should return 3
matchIt(["smash", "state", "smash", "bigots"], "smash") should return 2

"""

"""
Solve the task below this line
"""


def matchIt(l, value):
    return 0


"""
But above this line
"""


class TaskTests(unittest.TestCase):
    def test_matchIt_numbers(self):
        self.assertEqual(3, matchIt([1, 2, 3, 4, 1, 1], 1))

    def test_matchIt_strings(self):
        self.assertEqual(2, matchIt(["smash", "state", "smash", "bigots"], "smash"))


if __name__ == '__main__':
    unittest.main()
