import unittest
import sys

"""

Your first task is as follows:

* Create a variable called myVariable, and set the value to "Hello!"
* Calculate two to the power of 50, and save the result in the variable myNumber

Verify that you've done this correctly by running the following from the command line:
python task1.py

Description of needed functionality
To assign a variable you must use the =-operator.

So if you do
a = 10
You'll have a variable a set to the value 10.

The operator for doing x to the power of y is **, so 5**3 is 5*5*5 (or 125).

Strings have to be declared using quotes (" or ').
a = "this is a string"
If you don't use quotes, python will interpret it as variables rather than a string.
"""

"""
Solve the task below this line
"""

"""
But above this line
"""


class TaskTests(unittest.TestCase):
    def test_isMyVariableSet(self):
        self.assertEqual("Hello!", myVariable)

    def test_isMyNumberSet(self):
        self.assertEqual(2**50, myNumber)


if __name__ == '__main__':
    unittest.main()
