import unittest

"""

Task 3:

* Create a function called ping that takes no arguments and returns the word "pong"
* Create a function called addTogether that takes two arguments and returns them added together

ping() should return "pong"
addTogether(3, 8) should return 11
addTogether("foo", "bar") should return "foobar"

This task requires only the knowledge described in task2.py

"""

"""
Solve the task below this line
"""

"""
But above this line
"""


class TaskTests(unittest.TestCase):
    def test_ping(self):
        self.assertEqual("pong", ping())

    def test_addTogether_numbers(self):
        self.assertEqual(8, addTogether(3, 5))

    def test_addTogether_string(self):
        self.assertEqual("aaB", addTogether("aa", "B"))


if __name__ == '__main__':
    unittest.main()
